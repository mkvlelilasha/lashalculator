package com.example.lashalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private var operand:Double = 0.0
    private var operation = ""
    private var dot = "."


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)
    }


    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {

            var result = resultTextView.text.toString()
            var number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }

            resultTextView.text = result + number

        }
    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView) {

            var operand = resultTextView.text.toString()
            this.operand = operand.toDouble()

            operation = clickedView.text.toString()
            resultTextView.text = ""

        }
    }

    fun equalsClick(clickedView: View) {

        if (clickedView is TextView) {

            val secOperand = resultTextView.text.toString().toDouble()

            when (operation) {
                "+" -> resultTextView.text = (operand + secOperand).toString()
                "-" -> resultTextView.text = (operand - secOperand).toString()
                "*" -> resultTextView.text = (operand * secOperand).toString()
                "/" -> resultTextView.text = (operand / secOperand).toString()
            }



            if (resultTextView.text.toString().toDouble() % 1 == 0.0) {


                resultTextView.text = resultTextView.text.dropLast(2)

            }

        }
    }

    fun clearclick(clickedView: View){

        if (clickedView is TextView) {

            resultTextView.text = ""

        }

    }

    fun backspaceclick(clickedView: View){

        if (clickedView is TextView) {

            val sigrdze = resultTextView.length()

            if (sigrdze > 0){
                resultTextView.text = resultTextView.text.subSequence(0, sigrdze - 1)
            }

        }

    }

    fun dotclick(clickedView: View){

        if (clickedView is TextView) {

            val rame = resultTextView.text.toString()
            val rume = dot

            resultTextView.text = rame.plus(rume)


        }
    }


}

